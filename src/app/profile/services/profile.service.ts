import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';
import { IPokemon } from 'src/app/catalogue/models/pokemon.model';
import { StorageKeys } from 'src/app/login/enums/storage-keys.enum';
import { ITrainer } from 'src/app/login/models/trainer.model';
import { StorageUtil } from 'src/app/login/services/utils/storage.util';
import { environment } from 'src/environments/environment';

const { apiTrainers, ANGULAR_API_KEY } = environment

const headers = new HttpHeaders({
  "content-type": "application/json",
  "x-api-key": ANGULAR_API_KEY
})


@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private readonly http: HttpClient) { }

  //function to map pokemons from catalogue to profile if they matched
  get catchPokemons() {
    let iPokemons: IPokemon[] = [];
    let allPokes = StorageUtil.read<IPokemon[]>(StorageKeys.Pokemons);
    if (allPokes) {
      let allCought = StorageUtil.read<ITrainer>(StorageKeys.Trainer)?.pokemon;
      for (let i = 0; i < allPokes?.length; i++) {
        for (let j = 0; j < allCought!.length; j++) {
          if (allPokes[i].name === allCought![j]) {
            iPokemons.push(allPokes[i]);
            break;
          }
        }
      }
    }
    return iPokemons;
  }

  //function to add pokemon to api and storage
  addPokemon(pokemon: string) {
    const trainer: ITrainer | undefined = StorageUtil.read(StorageKeys.Trainer);
    if (trainer) {
      let pokemons = trainer.pokemon;
      if (pokemons.indexOf(pokemon) === -1) {
        pokemons.push(pokemon)
        trainer.pokemon = pokemons;
        return this.http.patch<ITrainer>(apiTrainers + "/" + trainer.id,
          trainer,
          {
            headers
          })
          .pipe(
            map((response: ITrainer) => {
              return response;
            })
          )
          .subscribe({
            next: (pokemons: ITrainer) => {
              StorageUtil.save(StorageKeys.Trainer, pokemons)
              //update storage with new pokemons
            },
            error: (error: HttpErrorResponse) => {
              console.log(error.message)
            }
          })
      }
      console.log("addPokemon: already catched this pokemon!")
    }
    return null
  }


  //funcction to remove pokemon from api 
  removePokemon(pokemon: string) {
    const trainer: ITrainer | undefined = StorageUtil.read(StorageKeys.Trainer);
    if (trainer) {
      let pokemons = trainer.pokemon;
      let updatedPokemons = []
      for (let i = 0; i < pokemons.length; i++) {
        if (pokemons[i] !== pokemon) updatedPokemons.push(pokemons[i])
      }
      trainer.pokemon = updatedPokemons;
      return this.http.patch<ITrainer>(apiTrainers + "/" + trainer.id,
        trainer,
        {
          headers
        })
        .pipe(
          map((response: ITrainer) => {
            return response;
          })
        )
        .subscribe({
          next: (pokemons: ITrainer) => {
            StorageUtil.save(StorageKeys.Trainer, pokemons)
          },
          error: (error: HttpErrorResponse) => {
            console.log(error.message)
          }
        })
    }
    return null
  }

}
