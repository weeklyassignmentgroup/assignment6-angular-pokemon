import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IPokemon } from 'src/app/catalogue/models/pokemon.model';
import { StorageKeys } from 'src/app/login/enums/storage-keys.enum';
import { ITrainer } from 'src/app/login/models/trainer.model';
import { TrainerService } from 'src/app/login/services/trainer.service';
import { StorageUtil } from 'src/app/login/services/utils/storage.util';
import { ProfileService } from '../../services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css']
})
export class ProfilePage implements OnInit {


  get pokemons() {
    return this.profileService.catchPokemons;
  }

  constructor(private readonly profileService: ProfileService,
    private readonly router: Router,
    private readonly trainerService: TrainerService) { }

  ngOnInit(): void {
  }

  handleLogout() {
    sessionStorage.removeItem(StorageKeys.Trainer);
    sessionStorage.removeItem(StorageKeys.Pokemons);
    this.trainerService.trainer = undefined;
    this.router.navigateByUrl("/login")
  }
}
