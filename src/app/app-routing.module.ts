import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CataloguePage } from './catalogue/pages/catalogue/catalogue.page';
import { LoginPage } from './login/pages/login/login.page';
import { AuthGuard } from './login/guards/auth.guard';
import { ProfilePage } from './profile/pages/profile/profile.page';

const routes: Routes = [
  { path: 'login', component: LoginPage },
  { path: 'catalogue', component: CataloguePage, canActivate: [AuthGuard] },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: "profile", component: ProfilePage, canActivate: [AuthGuard] }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
