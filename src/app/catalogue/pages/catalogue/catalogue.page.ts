import { Component, OnInit } from '@angular/core';
import { StorageKeys } from 'src/app/login/enums/storage-keys.enum';
import { StorageUtil } from 'src/app/login/services/utils/storage.util';
import { IPokemon } from '../../models/pokemon.model';
import { PokemonCatalogueService } from '../../services/pokemon-catalogue.service';

@Component({
  selector: 'app-catalogue-page',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})
export class CataloguePage implements OnInit {

  constructor(
    private readonly pokeCataService: PokemonCatalogueService
  ) { }



  get pokemons(): IPokemon[] {

    let pokemonsRead = StorageUtil.read<IPokemon[]>(StorageKeys.Pokemons);
    if (pokemonsRead !== undefined) {
      return pokemonsRead;
    }
    return this.pokeCataService.pokemons;
  }

  get loading(): boolean {
    return this.pokeCataService.loading;
  }

  get error(): string {
    return this.pokeCataService.error;
  }


  ngOnInit(): void {

    if (!StorageUtil.read(StorageKeys.Pokemons)) {
      this.pokeCataService.findFirstPageOfPokemon();
    }

  }

}
