
//Interface for pokemon object
export interface IPokemon {
  name : string,
  url : string,
  pictureUrl: string,
  id : number
}