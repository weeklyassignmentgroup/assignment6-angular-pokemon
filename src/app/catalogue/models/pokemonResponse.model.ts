import { IPokemon } from "./pokemon.model";

//Interface for recieving data from pokemonAPI
export interface PokemonResponse{
  count : number,
  next: string | null,
  previous: string | null,
  results: IPokemon[]
}