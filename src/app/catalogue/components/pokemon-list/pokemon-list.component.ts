import { Component, Input, OnInit } from '@angular/core';
import { ProfileService } from 'src/app/profile/services/profile.service';
import { IPokemon } from '../../models/pokemon.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  @Input() pokemons: IPokemon[] = [];

  @Input() catch: boolean = false;

  get caughtPokemon() {
    return this.profService.catchPokemons;
  }


  constructor(private readonly profService: ProfileService) {
  }

  ngOnInit(): void {
  }

  //params: pokemon name
  //adds pokemon to storage and patches api
  addPokemon(pokemon: string) {
    this.profService.addPokemon(pokemon);
  }

  //params: pokemon name
  //removes pokemon to storage and patches api
  removePokemon(pokemon: string) {
    this.profService.removePokemon(pokemon);

  }

}
