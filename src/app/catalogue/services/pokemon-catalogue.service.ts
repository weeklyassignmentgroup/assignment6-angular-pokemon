import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { finalize, map } from 'rxjs';
import { StorageKeys } from 'src/app/login/enums/storage-keys.enum';
import { StorageUtil } from 'src/app/login/services/utils/storage.util';
import { environment } from 'src/environments/environment';
import { IPokemon } from '../models/pokemon.model';
import { PokemonResponse } from '../models/pokemonResponse.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {


  @Output() loadedImages: EventEmitter<void> = new EventEmitter();

  private _pokemons: IPokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  constructor(private readonly http: HttpClient) { }

  get pokemons(): IPokemon[] {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  public findFirstPageOfPokemon(): void {
    console.log("Fire only if need to get pokenmon");

    this._loading = true;
    this.http.get<PokemonResponse>(environment.apiPokemon)
      .pipe(
        map((response: PokemonResponse) => {
          let parsedPokemons: IPokemon[] = [];
          response.results.forEach((pokemon: IPokemon) => {
            pokemon.id = this.getIdFromUrl(pokemon.url);
            pokemon.pictureUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`
            parsedPokemons.push(pokemon);
          })
          return parsedPokemons;
        }),
        finalize(() => {
          this._loading = false;
        })
      )
      .subscribe({
        next: (pokemons: IPokemon[]) => {
          this._pokemons = pokemons;
          StorageUtil.save<IPokemon[]>(StorageKeys.Pokemons, pokemons!);
          this.loadedImages.emit();
        },
        error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })

  }


  private getIdFromUrl(url: string): number {
    const reg = new RegExp(/((?<=pokemon[/])(\d+)(?=[/]))/g);
    var id = url.match(reg);
    if (!id) return 0;

    return Number.parseInt(id[0]);
  }



}
