import { Injectable } from '@angular/core';
import { StorageKeys } from 'src/app/login/enums/storage-keys.enum';
import { StorageUtil } from 'src/app/login/services/utils/storage.util';
import { IPokemon } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  private _pokemons?: IPokemon[]

  get pokemons(): IPokemon[] | undefined {
    return this._pokemons;
  }

  set pokemons(pokemons: IPokemon[] | undefined) {
    StorageUtil.save<IPokemon[]>(StorageKeys.Pokemons, pokemons!);
    this._pokemons = pokemons;
  }

  constructor() {
    const storedPokemons: IPokemon[] | undefined = StorageUtil.read<IPokemon[]>(StorageKeys.Pokemons);
    this._pokemons = storedPokemons;
  }
}
