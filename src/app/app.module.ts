import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CataloguePage } from './catalogue/pages/catalogue/catalogue.page';
import { LoginFormComponent } from './login/components/login-form/login-form.component';
import { HttpClientModule } from "@angular/common/http";
import { LoginPage } from './login/pages/login/login.page';
import { PokemonListComponent } from './catalogue/components/pokemon-list/pokemon-list.component';
import { FormsModule } from '@angular/forms';
import { ProfilePage } from './profile/pages/profile/profile.page';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    CataloguePage,
    LoginFormComponent,
    ProfilePage,
    PokemonListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
