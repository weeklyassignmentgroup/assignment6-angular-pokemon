export class StorageUtil{

  public static save<T>(key: string, value: T) : void {
    sessionStorage.setItem(key, JSON.stringify(value));
  }

  public static read<T>(key: string) : T | undefined  {
    const storedValue = sessionStorage.getItem(key);
    try{
      return (storedValue) ? JSON.parse(storedValue) as T :  undefined;
    }
    catch(e){
      sessionStorage.removeItem(key);
      return undefined;
    }
  }

}