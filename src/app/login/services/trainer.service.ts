import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { ITrainer } from '../models/trainer.model';
import { StorageUtil } from './utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  private _trainer?: ITrainer;

  get trainer(): ITrainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: ITrainer | undefined) {
    StorageUtil.save<ITrainer>(StorageKeys.Trainer, trainer!)
    this._trainer = trainer;
  }


  constructor() {
    const storedTrainer: ITrainer | undefined = StorageUtil.read<ITrainer>(StorageKeys.Trainer);
    this._trainer = storedTrainer;
  }

}
