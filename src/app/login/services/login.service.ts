import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ITrainer } from '../models/trainer.model';


const apiTrainers = environment.apiTrainers;

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  constructor(private readonly http: HttpClient) { }

  
  login(username: string): Observable<ITrainer> {
    return this.checkUsername(username)
      .pipe(
        switchMap((trainer: ITrainer | undefined) => {
          if (trainer === undefined) {
            return this.createUser(username);
          }
          return of(trainer);
        }),
      )
  }

  private checkUsername(username: string): Observable<ITrainer | undefined> {
    return this.http.get<ITrainer[]>(`${apiTrainers}?username=${username}`)
      .pipe(
        map((response: ITrainer[]) => {
          return response.pop();
        })
      )
  }

  private createUser(username: string): Observable<ITrainer> {
    const trainer = {
      username,
      pokemon: []
    };



    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "X-API-KEY": environment.ANGULAR_API_KEY,
    });

    return this.http.post<ITrainer>(apiTrainers, trainer, {
      headers
    })

  }


}
