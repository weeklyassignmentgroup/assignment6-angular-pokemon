import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from '../../enums/storage-keys.enum';
import { ITrainer } from '../../models/trainer.model';
import { StorageUtil } from '../../services/utils/storage.util';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.css']
})
export class LoginPage implements OnInit {

  constructor(private readonly router: Router) { }

  ngOnInit(): void {
    if (StorageUtil.read<ITrainer>(StorageKeys.Trainer)) {
      this.handleLogin();
    }
  }

  handleLogin(): void {
    this.router.navigateByUrl("/catalogue")
  }

}
