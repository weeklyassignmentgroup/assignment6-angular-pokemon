export interface ITrainer {
  id: number;
  username: string;
  pokemon: string[];
}

