import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ITrainer } from '../../models/trainer.model';
import { LoginService } from '../../services/login.service';
import { TrainerService } from '../../services/trainer.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  @Output() login: EventEmitter<void> = new EventEmitter();


  constructor(private readonly router: Router,
    private readonly loginService: LoginService,
    private readonly TrainerService: TrainerService) { }
  ngOnInit(): void {
  }

  handleOnSubmit(loginNgForm: NgForm) {
    const { username } = loginNgForm.value;

    this.loginService.login(username)
      .subscribe({
        next: (trainer: ITrainer) => {
          this.TrainerService.trainer = trainer;
          this.login.emit();
        },
        error: () => {

        }
      })

  }
}
