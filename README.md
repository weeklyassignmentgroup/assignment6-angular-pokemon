# Assignment6Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.3.


## Description
We've built a Single-Page Application for catching pokemons using Angular Framework.
The web application has three main pages --> 
* /login
* /catalogue
* /profile
Users are created whenever you try to login with a new username.
Logging in will bring you to the catalogue page.
Here you can catch any of the pokemons.
You will be able to see your catched pokemons inside your profile page.

## Component trees
![original-component-tree](./src/assets/angular-ComponentTree.pdf)

[updated-component-tree](./src/assets/updatedComponentTree.png)


## Contributors
* [Øyvind Reitan](https://gitlab.com/hindrance)
* [Eivind Bertelsen](https://gitlab.com/eivindTB)


## Usage

Published Webpage: [https://pokemon-gotta-catch-them-all.herokuapp.com/login]
Published API: [https://ove-noroff-api.herokuapp.com/trainers]


## Dependencies
Run `npm install` to install dependencies after git clone. 


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.


